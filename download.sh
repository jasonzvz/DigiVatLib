#!/bin/bash

## Configuration
DOWNLOAD_THREADS=20
STARTING_PAGE=1

## Checking
if [ $# != 1 ]; then
    echo "usage: $0 <identifier>"
    echo "<identifier>: the id of the book in https://digi.vatlib.it"
    echo ""
    echo "Example: $0 Borg.cin.468"
    echo "it will download image for https://digi.vatlib.it/view/MSS_Borg.cin.468"
    exit 1
fi

## Processing the uri
ID=$1
MANIFEST="https://digi.vatlib.it/iiif/MSS_${ID}/manifest.json"
IMAGE_URI_TEMPLATE="https://digi.vatlib.it/pub/digit/MSS_${ID}/iiif/${ID}_###BOOK_PAGE###.jp2/full/full/0/native.jpg"
TASK_LIST="./task_list_${ID}.txt"

echo "================ Total page num =================="
## get total page numbers and urls
curl -s ${MANIFEST} | jq -r '.sequences[].canvases[].images[].resource.service | .["@id"]' > ${TASK_LIST}
num_of_pages=$(cat ${TASK_LIST} | wc -l)
echo "=== Pages number: [${num_of_pages}]"
echo ""

## Start to download
if [ -n "num_of_pages" -a "$num_of_pages" -gt 0 ]; then
    if [ ! -e ${ID} ]; then
        mkdir ${ID}
    fi
    echo "========= Downloading configuration ============="
    echo "=== Download folder path: [${ID}]"
    echo "=== Download thread number: [${DOWNLOAD_THREADS}]"
    echo "=== Download pages: [${STARTING_PAGE} -> ${num_of_pages}]"
    echo ""
    echo "================== Download ======================"
    download_list=""
    count=1

    while read -r line
    do
        ## Default profile: region(full), size(full), rotation(0), quality(native), format(jpg)
        # We assume the default profile setting is correct for the download
        # Available profile options can be found in url in ($line/info.json)

        count_formatted=$(printf "%04d" ${count})
        page_url="$line/full/full/0/native.jpg"
        wget ${page_url} -O ${ID}/${count_formatted}.jpg -q &
        download_list="$download_list $count_formatted"
        if [ `expr $count % $DOWNLOAD_THREADS` -eq 0 -o ${count} -ge ${num_of_pages} ]; then
            # do something
            echo "Downloading pages: ${download_list}"
            wait
            echo "Done"
            download_list=""
        fi
        let count=count+1
    done < "${TASK_LIST}"
    echo "================== Success ======================="
else
    echo "Cannot find the page number of ${ID}"
    echo "================== Failed ========================"
fi

## Clean the tmp files
if [ -e "${TASK_LIST}" ]; then
    rm ${TASK_LIST}
fi
