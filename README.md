# DigiVatLib

To download the image from DigiVatLib online library using automatic scripts

## Background
The International Image Interoperability Framework (IIIF, pronounced “Triple-Eye-Eff”) is widely used for cultural heritage institutions, museums, libraries and archives. Due to its flexibility and functionalities, online libraries may use spliced image for better browsing experience, which is faster to load upon request. However, it will be hard for researcher to collect the database. This program is aims to download the full size image from [DigiVatLib](https://digi.vatlib.it/) for studies and researches. 

## Credits
Credits to Zhang Sinan and Wu Jinju for introducing such an intesting project.

## Usage
### Requirement to run the program
* This script is using shell script so we need a PC running Linux or Mac
* Let's take Ubuntu 16.04 as example, need to install package:
  - wget
  - curl
  - jq
* Download the git source code to local machine
  - run the script using the below script:
    ```
    $ ./download.sh [identifier]
    ```
  - For example, if you want to download all the page image of https://digi.vatlib.it/view/MSS_Borg.cin.468, run the script as below:
    ```
    $ ./download.sh Borg.cin.468
    ```

## How this script works
* This program will first find the number of pages of the book.
  This is by checking the https://digi.vatlib.it/iiif/MSS_${ID}/manifest.json information
* After the page number can be found. download the pages in multiple threads with the below url
  {scheme}://{server}{/prefix}/{identifier}/{region}/{size}/{rotation}/{quality}.{format}
* More IIIF API document can be found in [IIIF Image API 2.1.1](http://iiif.io/api/image/2.1)


## More configuration
(Need to modify the shell script)
* DOWNLOAD_THREADS: It would be very slow to download the image one by one, DOWNLOAD_THREADS is configured to be 20 by default, which means that it can download multiple images at the same time.
* STARTING_PAGE: It would be annoying to restart the download from scratch if accident happens, eg, power shortage, hurricane, blablabla, so if the download stops in the middle, just modify the STARTING_PAGE variable and run the download script again.

## Todo List
* [ ] Resume download by detect the last success download without modify the code
* [ ] Use Python instead of shell script for better multi-thread performance
* [ ] Allow user to select the download configuration after extracting the download option
